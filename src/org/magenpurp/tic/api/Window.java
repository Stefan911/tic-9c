package org.magenpurp.tic.api;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window {
    private static GraphicsDevice device = GraphicsEnvironment
            .getLocalGraphicsEnvironment().getScreenDevices()[0];
    private JFrame frame;

    public Window(String title) {
        frame = new JFrame(title);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1920, 1080);
        frame.setResizable(false);

    }

    public JFrame getFrame() {
        return frame;
    }

    public void close() {
        System.exit(0);
    }
}
